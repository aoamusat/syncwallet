const mongoose = require('mongoose');
require('dotenv').config();

/**
 * Connect to Database
 * @returns void
 */

const connectDB = async () => {
    try {
        const mongoUrl = process.env.MONGODB_URI;
        const connection = await mongoose.connect(mongoUrl, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log(`DB connected to: ${connection.connection.host}`);
    } catch (err) {
        console.log(err.message);
    }
};

module.exports = connectDB;
