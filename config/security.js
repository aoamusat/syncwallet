const crypto = require('crypto');

/**
 * Generates a random string
 * encoding can be either base64 or hex
 *
 * @param {String} encoding
 * @param {Number} length
 * @returns String
 */
const generateToken = (encoding = 'hex', length = 32) => {
    const key = crypto.randomBytes(64).toString(encoding).slice(0, length);
    return key;
};

module.exports = generateToken;
